output "lb_url" {
  value = module.ecs.lb_url
}

resource "local_file" "ecs_vars" {
  content = <<-EOT
   ${module.ecs.ecr_url}
   ${module.ecs.taks_definition_name}
   ${module.ecs.cluster_name}
   ${module.ecs.sercive_name}
   EOT
  filename = "ecs_vars"
}
