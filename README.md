# terraform
In this project we will create an ECS cluster with task definition, service 
and load balncer to expose our app.

Output of the this project will crate a file with ecs vars to use as part of [CI/CD](https://gitlab.com/galyakir1/hello-word-nodejs) process.


## Configuration:
1. Create an s3 bucket: 
```
aws s3api create-bucket --bucket <bucket-name> --region <region>
```
2. Upload tfvars to the bucket into folder vars:
```
aws s3 cp terraform.tfvars.json s3://$BUCKET_NAME/vars/terraform.tfvars.json
```
3. Update the backend at provider.tf:
 ```
  backend "s3" {
    bucket = "<bucket-name>"
    key = "terraform/terraform.tfstate"
    region = "<region>"
  }
```
 
4. Add those variables to CICD Variables:
```
AWS_ACCESS_KEY_ID
AWS_DEFAULT_REGION
AWS_SECRET_ACCESS_KEY
BUCKET_NAME
```

5. Start a new pipeline