variable "region" {
  description = "EC2 region"
  type = string
}

variable "tag_name" {
  description = "Tag name"
  type = string
}

variable "cluster_name" {
  description = "Name of the cluster"
  type = string
}

variable "vpc_cidr" {
  description = "VPC CIDR"
  type = string
}

variable "public_subnets" {
  description = "Public subnets list"
  type = list(any)
}

variable "availability_zones" {
  description = "availability zones subnets"
  type = list(any)
}

variable "app_port" {
  description = "Application port"
  type = string
}



