#this module create vpc with 3 subnets that have internet getway.
module "network" {
  source = "./modules/network"
  vpc_cidr = var.vpc_cidr
  public_subnets = var.public_subnets
  tag_name = var.tag_name
  availability_zones = var.availability_zones
}

module "ecs" {
  source = "./modules/ecs"
  public_subnets = module.network.public_subnets
  vpc_id = module.network.app_vpc.id
  region = var.region
  tag_name = var.tag_name
  app_port = var.app_port
}




