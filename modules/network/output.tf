output "app_vpc" {
  value = aws_vpc.app_vpc
}

output "public_subnets" {
  value = aws_subnet.public
}
