variable "vpc_cidr" {
  description = "VPC CIDR"
}

variable "public_subnets" {
  description = "Public subnets list"
}

variable "tag_name" {
  description = "Tag name"
}

variable "availability_zones" {
  description = "availability zones subnets"
}
