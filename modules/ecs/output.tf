output "lb_url" {
  value = aws_alb.application_load_balancer.dns_name
}

output "ecr_url" {
  value = aws_ecr_repository.aws-ecr.repository_url
}

output "taks_definition_name" {
  value = aws_ecs_task_definition.aws-ecs-task.family
}

output "cluster_name" {
  value = aws_ecs_cluster.aws-ecs-cluster.name
}

output "sercive_name" {
  value = aws_ecs_service.aws-ecs-service.name
}


