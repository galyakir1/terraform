variable "tag_name" {
  description = "Tag name"
}

variable "region" {
  description = "EC2 region"
  type = string
}

variable "public_subnets" {
  description = "Public subnets"
}

variable "vpc_id" {
  description = "Vpc id"
}

variable "app_port" {
  description = "Application port"
}



