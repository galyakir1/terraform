# ECR repository
# Resource used: aws_ecr_repository

resource "aws_ecr_repository" "aws-ecr" {
  name = "${var.tag_name}-ecr"
  tags = {
    Name        = "${var.tag_name}-ecr"
  }
}